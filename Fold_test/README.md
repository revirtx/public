## Sample code and data for RNA secondary structure prediction ##

* Fold single process code
* Fold-smp multi process code
* Fold-cuda CPU-based code 

```
#set up enivronment
export DATAPATH=./data_tables/
./Fold ENST00000371953.8-five_prime_UTR-rna.fa s1.ct -s s1.out 
./Fold-smp ENST00000371953.8-five_prime_UTR-rna.fa s2.ct -s s2.out
./Fold-cuda ENST00000371953.8-five_prime_UTR-rna.fa s3.out
```
